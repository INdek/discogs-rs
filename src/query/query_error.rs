// Copyright (C) 2018 The discogs-rs developers.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use serde_json;
use hyper;
use std;

/// `QueryError` is a structure of all the errors
/// that are possible during a query
// TODO: Document when these errors would occur
#[derive(Debug)]
pub enum QueryError {
    AuthenticationMissingError {
        reason: String,
    },
    JsonDecodeError {
        serde_err: Option<serde_json::Error>
    },
    HyperSendError {
        hyper_err: hyper::Error
    },
    HyperStatusError {
        response: hyper::client::response::Response
    },
    EmptyResponseError,
    TextReadError {
        error: std::io::Error
    }
}
