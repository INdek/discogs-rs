// Copyright (C) 2018 The discogs-rs developers.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.


pub mod query_error;
pub mod query_builder;
pub mod query_token_auth;
pub mod query_ks_auth;

pub use self::query_error::QueryError;
pub use self::query_builder::QueryBuilder;

//TODO: Put these under the module auth
pub use self::query_token_auth::DiscogsTokenAuth;
pub use self::query_ks_auth::DiscogsKSAuth;
