// Copyright (C) 2018 The discogs-rs developers.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use query::QueryError;
use query::*;
use hyper;
use hyper::header::*;
use hyper::status::StatusCode;
use hyper::net::HttpsConnector;
use hyper_native_tls::NativeTlsClient;
use std::io::Read;

pub trait QueryBuilder {
    fn get_key(&self) -> Option<String> {
        None
    }

    fn get_secret(&self) -> Option<String> {
        None
    }

    // returns the  url to perform the query
    fn get_query_url(&self) -> String;

    fn get_user_agent(&self) -> String;

    //TODO: Fix the unwrap()
    fn perform_request(&self) -> Result<String, QueryError> {
        let ssl = NativeTlsClient::new().unwrap();
        let connector = HttpsConnector::new(ssl);
        let client = hyper::Client::with_connector(connector);
        let mut request = client.get(self.get_query_url().as_str())
                                .header(UserAgent(self.get_user_agent()));

        if self.get_key().is_some() && self.get_secret().is_some() {
            request = request.header(Authorization(DiscogsKSAuth {
                key: self.get_key().unwrap(),
                secret: self.get_secret().unwrap()
            }));
        }

        let response = request.send();

        match response {
            Ok(mut text) => {
                let mut json: String = "".to_owned();

                if text.status != StatusCode::Ok {
                    return Err(QueryError::HyperStatusError {
                        response: text
                    });
                }
                let text_read_result = text.read_to_string(&mut json);

                if let Ok(sz) = text_read_result {
                    if sz <= 0 {
                        return Err(QueryError::EmptyResponseError);
                    }
                    return Ok(json);
                } else {
                    return Err(QueryError::TextReadError {
                        error: text_read_result.err().unwrap()
                    });
                }


            },
            Err(error) => return Err(QueryError::HyperSendError {
                hyper_err: error
            })
        }
    }
}
