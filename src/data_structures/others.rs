// Copyright (C) 2018 The discogs-rs developers.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use data_structures::*;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Community {
    pub contributors: Vec<Contributor>,
    pub data_quality: String,
    pub have: u32,
    pub rating: Rating,
    pub status: Status,
    pub submitter: Contributor,
    pub want: u32,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Rating {
    pub average: f32,
    pub count: u32,
}


#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ReleaseFormat {
    pub descriptions: Vec<String>,
    pub name: String,
    pub qty: String,
}


#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Identifier {
    // TODO: can we do an enum for this?
    #[serde(rename = "type")]
    pub identifier_type: String,
    pub value: String,
}


#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Track {
    pub duration: String,
    pub position: String,
    pub title: String,
    pub type_: String,
    pub extra_artists: Option<Vec<Artist>>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Video {
    pub description: String,
    pub duration: u32,
    pub embed: bool,
    pub title: String,
    pub uri: String,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub enum Status {
    Accepted,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub enum DataQuality {
    Correct,
    #[serde(rename="Needs Vote")]
    NeedsVote,
    #[serde(rename="Complete and Correct")]
    CompleteAndCorrect,
}

