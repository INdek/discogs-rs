// Copyright (C) 2018 The discogs-rs developers.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use data_structures::*;
use query::*;
use serde_json;
use std::collections::HashMap;
use itertools::Itertools;

/// The default API Endpoint
const SEARCH_ENDPOINT: &'static str = "/database";


#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum SearchResult  {
    Artist {
        data: Artist
    },
    Master {
        data: Master
    },
    Label {
        data: Label
    },
    Release {
        data: Release
    }
}

pub enum SearchType {
    Artist,
    Master,
    Label,
    Release
}

#[derive(Deserialize, Debug)]
struct SearchResultFull {
    pub pagination: Pagination,
    pub results: Vec<SearchResult>,
}


impl SearchType {
    pub fn to_string(&self) -> String {
        match *self {
            SearchType::Artist => "artist".to_string(),
            SearchType::Master => "master".to_string(),
            SearchType::Label => "label".to_string(),
            SearchType::Release => "release".to_string()
        }
    }
}

pub struct SearchQueryBuilder {
    api_endpoint: String,
    user_agent: String,

    /// The key and secret are required, however to keep
    /// a consistent styling across multiple QueryBuilders
    /// we wont require them on `new()`
    ///
    /// If none is provided the query will fail with a
    /// `QueryError::AuthenticationMissingError`
    key: Option<String>,
    secret: Option<String>,

    parameters: HashMap<String, String>,
}


impl SearchQueryBuilder {
    /// Creates a new instance of `SearchQueryBuilder`
    ///
    /// # Examples
    ///
    /// ```
    /// use discogs::data_structures::SearchQueryBuilder;
    ///
    /// let sqb = SearchQueryBuilder::new(discogs::API_URL.to_string(),
    ///                                   "USER_AGENT".to_string(),
    ///                                   Some("CLIENT_KEY".to_string()),
    ///                                   Some("CLIENT_SECRET".to_string()));
    /// ```
    pub fn new(api_endpoint: String,
               user_agent: String,
               key: Option<String>,
               secret: Option<String>) -> SearchQueryBuilder {
        SearchQueryBuilder {
            api_endpoint: api_endpoint,
            user_agent: user_agent,
            key: key,
            secret: secret,
            parameters: HashMap::new(),
        }
    }

    /// Set the query text to be sent in the query
    ///
    /// # Examples
    ///
    /// ```
    /// use discogs::data_structures::SearchQueryBuilder;
    ///
    /// let sqb = SearchQueryBuilder::new(discogs::API_URL.to_string(),
    ///                                   "USER_AGENT".to_string(),
    ///                                   Some("CLIENT_KEY".to_string()),
    ///                                   Some("CLIENT_SECRET".to_string()))
    ///                                    .query("query".to_string());
    /// ```
    pub fn query(mut self, query: String) -> Self {
        self.parameters.insert("query".to_string(), query);
        self
    }

    /// Set the search type of the query
    ///
    /// # Examples
    ///
    /// ```
    /// use discogs::data_structures::SearchQueryBuilder;
    /// use discogs::data_structures::SearchType;
    ///
    /// let sqb = SearchQueryBuilder::new(discogs::API_URL.to_string(),
    ///                                   "USER_AGENT".to_string(),
    ///                                   Some("CLIENT_KEY".to_string()),
    ///                                   Some("CLIENT_SECRET".to_string()))
    ///                                    .search_type(SearchType::Artist);
    /// ```
    pub fn search_type(mut self, search_type: SearchType) -> Self {
        self.parameters.insert("search_type".to_string(), search_type.to_string());
        self
    }


    /// Set the year in the query to be sent
    ///
    /// # Examples
    ///
    /// ```
    /// use discogs::data_structures::SearchQueryBuilder;
    ///
    /// let sqb = SearchQueryBuilder::new(discogs::API_URL.to_string(),
    ///                                   "USER_AGENT".to_string(),
    ///                                   Some("CLIENT_KEY".to_string()),
    ///                                   Some("CLIENT_SECRET".to_string()))
    ///                                    .year(1980);
    /// ```
    pub fn year(mut self, year: i32) -> Self {
        self.parameters.insert("year".to_string(), year.to_string());
        self
    }

    /// Set the title in the query to be sent
    ///
    /// # Examples
    ///
    /// ```
    /// use discogs::data_structures::SearchQueryBuilder;
    ///
    /// let sqb = SearchQueryBuilder::new(discogs::API_URL.to_string(),
    ///                                   "USER_AGENT".to_string(),
    ///                                   Some("CLIENT_KEY".to_string()),
    ///                                   Some("CLIENT_SECRET".to_string()))
    ///                                    .title("title".to_string());
    /// ```
    pub fn title(mut self, title: String) -> Self {
        self.parameters.insert("title".to_string(), title);
        self
    }

    /// Set the release_title in the query to be sent
    ///
    /// # Examples
    ///
    /// ```
    /// use discogs::data_structures::SearchQueryBuilder;
    ///
    /// let sqb = SearchQueryBuilder::new(discogs::API_URL.to_string(),
    ///                                   "USER_AGENT".to_string(),
    ///                                   Some("CLIENT_KEY".to_string()),
    ///                                   Some("CLIENT_SECRET".to_string()))
    ///                                    .release_title("release_title".to_string());
    /// ```
    pub fn release_title(mut self, release_title: String) -> Self {
        self.parameters.insert("release_title".to_string(), release_title);
        self
    }

    /// Set the credit in the query to be sent
    ///
    /// # Examples
    ///
    /// ```
    /// use discogs::data_structures::SearchQueryBuilder;
    ///
    /// let sqb = SearchQueryBuilder::new(discogs::API_URL.to_string(),
    ///                                   "USER_AGENT".to_string(),
    ///                                   Some("CLIENT_KEY".to_string()),
    ///                                   Some("CLIENT_SECRET".to_string()))
    ///                                    .credit("credit".to_string());
    /// ```
    pub fn credit(mut self, credit: String) -> Self {
        self.parameters.insert("credit".to_string(), credit);
        self
    }

    /// Set the artist in the query to be sent
    ///
    /// # Examples
    ///
    /// ```
    /// use discogs::data_structures::SearchQueryBuilder;
    ///
    /// let sqb = SearchQueryBuilder::new(discogs::API_URL.to_string(),
    ///                                   "USER_AGENT".to_string(),
    ///                                   Some("CLIENT_KEY".to_string()),
    ///                                   Some("CLIENT_SECRET".to_string()))
    ///                                    .artist("artist".to_string());
    /// ```
    pub fn artist(mut self, artist: String) -> Self {
        self.parameters.insert("artist".to_string(), artist);
        self
    }

    /// Set the anv in the query to be sent
    ///
    /// # Examples
    ///
    /// ```
    /// use discogs::data_structures::SearchQueryBuilder;
    ///
    /// let sqb = SearchQueryBuilder::new(discogs::API_URL.to_string(),
    ///                                   "USER_AGENT".to_string(),
    ///                                   Some("CLIENT_KEY".to_string()),
    ///                                   Some("CLIENT_SECRET".to_string()))
    ///                                    .anv("anv".to_string());
    /// ```
    pub fn anv(mut self, anv: String) -> Self {
        self.parameters.insert("anv".to_string(), anv);
        self
    }

    /// Set the label in the query to be sent
    ///
    /// # Examples
    ///
    /// ```
    /// use discogs::data_structures::SearchQueryBuilder;
    ///
    /// let sqb = SearchQueryBuilder::new(discogs::API_URL.to_string(),
    ///                                   "USER_AGENT".to_string(),
    ///                                   Some("CLIENT_KEY".to_string()),
    ///                                   Some("CLIENT_SECRET".to_string()))
    ///                                    .label("label".to_string());
    /// ```
    pub fn label(mut self, label: String) -> Self {
        self.parameters.insert("label".to_string(), label);
        self
    }

    /// Set the genre in the query to be sent
    ///
    /// # Examples
    ///
    /// ```
    /// use discogs::data_structures::SearchQueryBuilder;
    ///
    /// let sqb = SearchQueryBuilder::new(discogs::API_URL.to_string(),
    ///                                   "USER_AGENT".to_string(),
    ///                                   Some("CLIENT_KEY".to_string()),
    ///                                   Some("CLIENT_SECRET".to_string()))
    ///                                    .genre("genre".to_string());
    /// ```
    pub fn genre(mut self, genre: String) -> Self {
        self.parameters.insert("genre".to_string(), genre);
        self
    }

    /// Set the style in the query to be sent
    ///
    /// # Examples
    ///
    /// ```
    /// use discogs::data_structures::SearchQueryBuilder;
    ///
    /// let sqb = SearchQueryBuilder::new(discogs::API_URL.to_string(),
    ///                                   "USER_AGENT".to_string(),
    ///                                   Some("CLIENT_KEY".to_string()),
    ///                                   Some("CLIENT_SECRET".to_string()))
    ///                                    .style("style".to_string());
    /// ```
    pub fn style(mut self, style: String) -> Self {
        self.parameters.insert("style".to_string(), style);
        self
    }

    /// Set the country in the query to be sent
    ///
    /// # Examples
    ///
    /// ```
    /// use discogs::data_structures::SearchQueryBuilder;
    ///
    /// let sqb = SearchQueryBuilder::new(discogs::API_URL.to_string(),
    ///                                   "USER_AGENT".to_string(),
    ///                                   Some("CLIENT_KEY".to_string()),
    ///                                   Some("CLIENT_SECRET".to_string()))
    ///                                    .country("country".to_string());
    /// ```
    pub fn country(mut self, country: String) -> Self {
        self.parameters.insert("country".to_string(), country);
        self
    }
    /// Set the format in the query to be sent
    ///
    /// # Examples
    ///
    /// ```
    /// use discogs::data_structures::SearchQueryBuilder;
    ///
    /// let sqb = SearchQueryBuilder::new(discogs::API_URL.to_string(),
    ///                                   "USER_AGENT".to_string(),
    ///                                   Some("CLIENT_KEY".to_string()),
    ///                                   Some("CLIENT_SECRET".to_string()))
    ///                                    .format("format".to_string());
    /// ```
    pub fn format(mut self, format: String) -> Self {
        self.parameters.insert("format".to_string(), format);
        self
    }

    /// Set the catno in the query to be sent
    ///
    /// # Examples
    ///
    /// ```
    /// use discogs::data_structures::SearchQueryBuilder;
    ///
    /// let sqb = SearchQueryBuilder::new(discogs::API_URL.to_string(),
    ///                                   "USER_AGENT".to_string(),
    ///                                   Some("CLIENT_KEY".to_string()),
    ///                                   Some("CLIENT_SECRET".to_string()))
    ///                                    .catno("catno".to_string());
    /// ```
    pub fn catno(mut self, catno: String) -> Self {
        self.parameters.insert("catno".to_string(), catno);
        self
    }

    /// Set the barcode in the query to be sent
    ///
    /// # Examples
    ///
    /// ```
    /// use discogs::data_structures::SearchQueryBuilder;
    ///
    /// let sqb = SearchQueryBuilder::new(discogs::API_URL.to_string(),
    ///                                   "USER_AGENT".to_string(),
    ///                                   Some("CLIENT_KEY".to_string()),
    ///                                   Some("CLIENT_SECRET".to_string()))
    ///                                    .barcode("barcode".to_string());
    /// ```
    pub fn barcode(mut self, barcode: String) -> Self {
        self.parameters.insert("barcode".to_string(), barcode);
        self
    }

    /// Set the track in the query to be sent
    ///
    /// # Examples
    ///
    /// ```
    /// use discogs::data_structures::SearchQueryBuilder;
    ///
    /// let sqb = SearchQueryBuilder::new(discogs::API_URL.to_string(),
    ///                                   "USER_AGENT".to_string(),
    ///                                   Some("CLIENT_KEY".to_string()),
    ///                                   Some("CLIENT_SECRET".to_string()))
    ///                                    .track("track".to_string());
    /// ```
    pub fn track(mut self, track: String) -> Self {
        self.parameters.insert("track".to_string(), track);
        self
    }

    /// Set the submitter in the query to be sent
    ///
    /// # Examples
    ///
    /// ```
    /// use discogs::data_structures::SearchQueryBuilder;
    ///
    /// let sqb = SearchQueryBuilder::new(discogs::API_URL.to_string(),
    ///                                   "USER_AGENT".to_string(),
    ///                                   Some("CLIENT_KEY".to_string()),
    ///                                   Some("CLIENT_SECRET".to_string()))
    ///                                    .submitter("submitter".to_string());
    /// ```
    pub fn submitter(mut self, submitter: String) -> Self {
        self.parameters.insert("submitter".to_string(), submitter);
        self
    }

    /// Set the contributor in the query to be sent
    ///
    /// # Examples
    ///
    /// ```
    /// use discogs::data_structures::SearchQueryBuilder;
    ///
    /// let sqb = SearchQueryBuilder::new(discogs::API_URL.to_string(),
    ///                                   "USER_AGENT".to_string(),
    ///                                   Some("CLIENT_KEY".to_string()),
    ///                                   Some("CLIENT_SECRET".to_string()))
    ///                                    .contributor("contributor".to_string());
    /// ```
    pub fn contributor(mut self, contributor: String) -> Self {
        self.parameters.insert("contributor".to_string(), contributor);
        self
    }

    /// Perform request
    ///
    /// # Examples
    ///
    /// ```rust,no_run
    /// use discogs::data_structures::SearchQueryBuilder;
    ///
    /// let sqb = SearchQueryBuilder::new(discogs::API_URL.to_string(),
    ///                                   "USER_AGENT".to_string(),
    ///                                   Some("CLIENT_KEY".to_string()),
    ///                                   Some("CLIENT_SECRET".to_string()))
    ///                                       .get();
    /// ```
    pub fn get(self) -> Result<Vec<SearchResult>, QueryError> {
        if self.key.is_none() || self.secret.is_none() {
            return Err(QueryError::AuthenticationMissingError {
                reason: "Missing either key or secret when perfoming search request".to_string(),
            })
        }


        let result: Result<String, QueryError> = self.perform_request();

        if let Err(error) = result {
            return Err(error);
        } else {
            let result_string = result.ok().unwrap();
            let result: Result<SearchResultFull, serde_json::Error> =
                serde_json::from_str(&result_string);

            if let Ok(srf) = result {
                return Ok(srf.results);
            } else {
                return Err(QueryError::JsonDecodeError {
                    serde_err: result.err()
                });
            }

        }
    }
}

impl QueryBuilder for SearchQueryBuilder {
    fn get_key(&self) -> Option<String> {
        self.key.clone()
    }

    fn get_secret(&self) -> Option<String> {
        self.secret.clone()
    }

    //api.discogs.com/database/search?q= is a valid query, so is
    //api.discogs.com/database/search?q=&year=1
    fn get_query_url(&self) -> String {
        let mut url = format!("{}{}/search?q=", self.api_endpoint, SEARCH_ENDPOINT);

        if self.parameters.is_empty() {
            return url;
        }

        url.push_str(self.parameters["query"].as_str());

        if self.parameters.len() > 1 {
            url.push_str("&");

            url.push_str(
                self.parameters.iter()
                               .filter(|&(p, _)| p != "query")
                               .map(|(p, v)| format!("{}={},", p, v))
                               .intersperse(",".to_string())
                               .collect::<String>()
                               .as_str());
        }

        url
    }

    fn get_user_agent(&self) -> String {
        self.user_agent.clone()
    }
}
