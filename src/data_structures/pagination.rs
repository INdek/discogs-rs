// Copyright (C) 2018 The discogs-rs developers.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

#[derive(Deserialize, Debug)]
pub struct Pagination {
    pub per_page: i16,
    pub page: i16,
    pub items: i64,
    pub pages: i16,
    pub urls: PaginationUrls,
}

#[derive(Deserialize, Debug)]
pub struct PaginationUrls {
    pub next: Option<String>,
    pub last: Option<String>,
}
