// Copyright (C) 2018 The discogs-rs developers.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

#![cfg_attr(feature = "nightly", feature(test))]

#![doc(test(attr(allow(unused_variables), deny(warnings))))]

extern crate hyper;
extern crate hyper_native_tls;
extern crate serde;

#[macro_use] extern crate serde_json;
#[macro_use] extern crate serde_derive;
#[macro_use] extern crate itertools;

#[cfg(test)]
extern crate mockito;

#[cfg(all(test, feature = "nightly"))]
extern crate test;

pub mod data_structures;
pub mod query;
pub mod discogs;

pub use discogs::*;
