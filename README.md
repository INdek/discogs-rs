# Discogs API client

[![Build Status](https://img.shields.io/travis/afonso360/discogs-rs/master.svg?style=flat-square)](https://travis-ci.org/afonso360/discogs-rs) [![Build status](https://img.shields.io/appveyor/ci/afonso360/discogs-rs/master.svg?style=flat-square)](https://ci.appveyor.com/project/afonso360/discogs-rs) [![Coverage Status](https://img.shields.io/coveralls/github/afonso360/discogs-rs/master.svg?style=flat-square)](https://coveralls.io/github/afonso360/discogs-rs?branch=master) [![License: MIT/Apache 2.0](https://img.shields.io/crates/l/discogs.svg?style=flat-square)]() [![Crates.io](https://img.shields.io/crates/v/discogs.svg?style=flat-square)](https://crates.io/crates/discogs)

The API is slowly becoming more stabilized and probably won't change very much.

# Tasks to do before 1.0.0
 - [ ] Implement all the API
    - [ ] Database
      - [ ] Release
        - [ ] Allow currency in requests
        - [ ] Community ratings
        - [ ] Get Ratings by username
        - [ ] Put Ratings by username
        - [ ] Delete Ratings by username
      - [ ] Master
        - [ ] Get Versions
      - [x] Label
        - [x] Get Releases
      - [x] Artist
        - [x] Get Releases
      - [x] Search
        - [x] Perform search
        - [x] Implement filters
    - [ ] Users
      - [ ] Profile
      - [ ] Identity
      - [ ] Submissions
      - [ ] Contributions
      - [ ] Collections
      - [ ] Wantlist
      - [ ] Lists
    - [ ] Marketplace
      - [ ] Inventory
      - [ ] Listings
      - [ ] Orders
      - [ ] Fee
      - [ ] Price Suggestions

 - [ ] Rate limit the api
 - [ ] Handle query error codes that are not 200
 - [ ] Write better tests
 - [ ] Fully document all files
 - [ ] Write some example code


If you would like to contribute please file an issue or pull request, even if its
to tell me that I have no documentation or tests!

## License
[license]: #license

Discogs-rs is primarily distributed under the terms of both the MIT license
and the Apache License (Version 2.0).

See [LICENSE-APACHE](LICENSE-APACHE), [LICENSE-MIT](LICENSE-MIT), and
[COPYRIGHT](COPYRIGHT) for details.
